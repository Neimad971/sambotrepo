//
//  ArtistService.m
//  deezcovery
//
//  Created by Damien TALBOT on 20/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "ArtistService.h"
#import "Artist.h"
#import "Album.h"
#import "Track.h"

@implementation ArtistService

static ArtistService *sharedInstance = nil;

+ (instancetype)sharedInstance{
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        if (sharedInstance == nil)
            sharedInstance = [[super allocWithZone:NULL] init];
    });
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedInstance];
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;
}


- (id)init
{
    if(nil != (self = [super init]))
    {}
    return self;
}


- (NSString *) getArtistId:(NSString *)searchInput
{
   

    NSString *capitalizedSearchInput = [searchInput uppercaseString];
    
    NSMutableString *urlWithString = [[NSMutableString alloc]initWithString:@"http://api.deezer.com/search/artist?q="];
    
    NSLog(@"NSMutableString : %@",urlWithString);
    [urlWithString appendString:[searchInput stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
     NSLog(@"NSMutableString 2 : %@",urlWithString);
    NSURL *url = [NSURL URLWithString:urlWithString ];
    
     NSLog(@"URL : %@",url);
    

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"GET"];
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:&urlResponse
                                                          error:&requestError];
    
    NSError *jsonError;
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:response
                                                         options:NSJSONReadingAllowFragments
                                                           error:&jsonError];
    

    NSDictionary *data = [JSON valueForKeyPath:@"data"];

    NSString *id = nil;
    
    for (NSString *currentValue in data)
    {
        for (NSString *key in currentValue)
        {
            if ([key isEqualToString:@"name"])
            {
                NSString *nameValue = [currentValue valueForKey:key];
                NSString *capitalizedNameValue = [nameValue uppercaseString];
                
                if( [capitalizedNameValue isEqualToString:capitalizedSearchInput] )
                {
                    id = [currentValue valueForKey:@"id"];
                    return id;
                }
            }
        }
    }
    
    return  id;
}



- (NSArray *) getArtistRelatedToAnotherArtist :(NSString *) otherArtistId
{

   NSMutableString *urlWithString = [[NSMutableString alloc]initWithString:@"http://api.deezer.com/artist/"];
   [urlWithString appendString:[otherArtistId description]];
   [urlWithString appendString:@"/related"];
    
    NSLog(@"%@",urlWithString);
    
    NSURL *url = [NSURL URLWithString:urlWithString];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"GET"];
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&urlResponse
                                                         error:&requestError];
    
    NSError *jsonError;
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:response
                                                         options:NSJSONReadingAllowFragments
                                                           error:&jsonError];
    
    //NSLog(@"%@",JSON );
    NSDictionary *data = [JSON valueForKeyPath:@"data"];
    
    NSMutableArray *artistRelatedTo = [[NSMutableArray alloc]init];
    Artist *artist = [[Artist alloc]init];
    
    for (NSString *currentValue in data)
    {
       artist = [[Artist alloc]init];
        
        for (NSString *key in currentValue)
        {
            if ([key isEqualToString:@"id"])
            {
                [artist setIdArtist:[currentValue valueForKey:key]];
            }
            
            if ([key isEqualToString:@"name"])
            {
                [artist setName:[currentValue valueForKey:key]];
            }
            
            if ([key isEqualToString:@"nb_album"])
            {
                [artist setNbAlbums:[[currentValue valueForKey:key] intValue]];
            }
            
            if ([key isEqualToString:@"nb_fan"])
            {
                [artist setNbFans:[[currentValue valueForKey:key] intValue]];
            }
            if ([key isEqualToString:@"picture"])
            {
                [artist setArtistImageURL:[currentValue valueForKey:key]];
            }

        }
    
        [artistRelatedTo addObject:artist];
        artist = nil;
       
    }
    
    
    return artistRelatedTo;
}

- (NSArray *) getArtistAlbums:(NSString *) idArtist
{
    NSMutableString *urlWithString = [[NSMutableString alloc]initWithString:@"http://api.deezer.com/artist/"];
    [urlWithString appendString:[idArtist description]];
    [urlWithString appendString:@"/albums"];
    
    NSLog(@"%@",urlWithString);
    
    NSURL *url = [NSURL URLWithString:urlWithString];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"GET"];
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&urlResponse
                                                         error:&requestError];
    
    NSError *jsonError;
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:response
                                                         options:NSJSONReadingAllowFragments
                                                           error:&jsonError];
    
    //NSLog(@"%@",JSON );
    NSDictionary *data = [JSON valueForKeyPath:@"data"];
    
    NSMutableArray *albums = [[NSMutableArray alloc]init];
    Album *album = [[Album alloc]init];
    
    for (NSString *currentValue in data)
    {
        album = [[Album alloc]init];
        
        for (NSString *key in currentValue)
        {
            if ([key isEqualToString:@"id"])
            {
                [album setIdAlbum:[currentValue valueForKey:key]];
            }
            
            if ([key isEqualToString:@"title"])
            {
                [album setAlbumName:[currentValue valueForKey:key]];
            }
            
            if ([key isEqualToString:@"tracklist"])
            {
                [album setTracklist:[currentValue valueForKey:key]];
            }
            if ([key isEqualToString:@"cover"])
            {
                [album setAlbumCoverURL:[currentValue valueForKey:key]];
            }
        }
        
        [albums addObject:album];
        album = nil;
        
    }
    
    return albums;
}


- (NSString *) getTracksAlbum:(NSString *) tracklistUrl
{
    NSURL *url = [NSURL URLWithString:tracklistUrl];
    NSLog(@"%@",tracklistUrl);
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"GET"];
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&urlResponse
                                                         error:&requestError];
    
    NSError *jsonError;
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:response
                                                         options:NSJSONReadingAllowFragments
                                                           error:&jsonError];
    
    //NSLog(@"%@",JSON );
    NSDictionary *data = [JSON valueForKeyPath:@"data"];
    
    
    
    
    NSMutableArray *tracklist = [[NSMutableArray alloc]init];
    Track *track = [[Track alloc]init];
    
    for (NSString *currentValue in data)
    {
        track = [[Track alloc]init];
        
        for (NSString *key in currentValue)
        {
            if ([key isEqualToString:@"id"])
            {
                [track setIdTrack:[currentValue valueForKey:key]];
            }
            
            if ([key isEqualToString:@"title"])
            {
                [track setTitle:[currentValue valueForKey:key]];
            }
            
            if ([key isEqualToString:@"duration"])
            {
                [track setDuration:[currentValue valueForKey:key]];
            }
            
            if ([key isEqualToString:@"preview"])
            {
                [track setPreview:[currentValue valueForKey:key]];
            }
        }
        
        [tracklist addObject:track];
        track = nil;
        
    }

    return tracklist;
}


@end
