//
//  main.m
//  deezcovery
//
//  Created by Damien TALBOT on 17/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
