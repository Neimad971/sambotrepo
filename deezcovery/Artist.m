//
//  Artist.m
//  deezcovery
//
//  Created by Damien TALBOT on 17/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "Artist.h"

@implementation Artist


- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        self.name = nil;
        self.nbAlbums = 0;
        self.nbFans = 0;
        self.artistImageURL = nil;
    }
    
    return self;
}

@end
