//
//  SessionManager.m
//  TodoList
//
//  Created by Julien Sarazin on 19/12/14.
//  Copyright (c) 2014 Julien Sarazin. All rights reserved.
//

#import "SessionManager.h"

#define API_BASE_URL @"http://169.254.242.64:8080"
@interface SessionManager () <NSURLSessionDelegate>
@property (strong, nonatomic) NSURLSession *currentSession;
@end

@implementation SessionManager
static SessionManager *sharedInstance = nil;

#pragma mark - Singleton Pattern -
+ (instancetype)sharedInstance{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        if (sharedInstance == nil)
            sharedInstance = [[super allocWithZone:NULL] init];
    });
    return sharedInstance;
}
+ (id)allocWithZone:(NSZone *)zone{
    return [self sharedInstance];
}
- (id)copyWithZone:(NSZone *)zone{
    return self;
}
- (id)init{
    if(nil != (self = [super init]))
    {
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.allowsCellularAccess = NO;
        [sessionConfig setHTTPAdditionalHeaders:@{@"Accept": @"application/json"}];
        [sessionConfig setHTTPAdditionalHeaders:@{@"Content-Type": @"application/json"}];
        [sessionConfig setHTTPCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        sessionConfig.timeoutIntervalForRequest = 30.0;
        sessionConfig.timeoutIntervalForResource = 60.0;
        
        self.currentSession = [NSURLSession sessionWithConfiguration:sessionConfig
                                                            delegate:self
                                                       delegateQueue:nil];
    }
    return self;
}

/**
 GET Request
 **/
- (void)GET:(NSString *)path completion:(void (^)(NSDictionary *))completion{
    
    NSURL *url = [NSURL URLWithString:[API_BASE_URL stringByAppendingString:path]];
    id completionHandler = ^(NSData *data,
                             NSURLResponse *response,
                             NSError *error) {
        if (error){
            NSLog(@"Request Failed. Reason: %@", error.localizedDescription);
            return;
        }
        
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
        if (httpResp.statusCode != 200){
            //NSLog(@"Unexcepected status code: %i", httpResp.statusCode);
            if (completion) completion(nil);
        }
        
        NSError *jsonError;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingAllowFragments
                                                               error:&jsonError];
        
        
        NSLog(@"%@", JSON);

        
        if (jsonError) {
            NSLog(@"Error during serializing JSON. Reason: %@", jsonError.localizedDescription);
            if (completion) completion(nil);
        }
        
        if(completion) completion(JSON);
    };
    
    NSLog(@"Making GET request for URL: %@", url);
    NSURLSessionDataTask *task = [self.currentSession dataTaskWithURL:url
                                                    completionHandler:completionHandler];
    
    [task resume];
}



/**
 LIST, EQUIVALENT OF GET BUT FOR COLLECTIONS, (PROVIDE AN NSARRAY AS RESPONSE OBJECT)
 **/
- (void)LIST:(NSString *)path completion:(void (^)(NSArray *))completion{
    
    [self GET:path completion:^(NSDictionary *JSON) {
        completion((NSArray *)JSON);
    }];
}

- (void) GETArtistId:(NSString *)path completion:(void (^)(NSDictionary *))completion
{
    NSLog(@"getArtistId");

    
    NSMutableString *urlWithString = [[NSMutableString alloc]initWithString:@"http://api.deezer.com/search/artist?q="]; //PATH
   // [urlWithString appendString:searchInput];
    
    NSURL *url = [NSURL URLWithString:urlWithString];
    
    id completionHandler = ^(NSData *data, NSURLResponse *response)
    {
    
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:10];
    
        [request setHTTPMethod: @"GET"];
    
    
        NSError *jsonError;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingAllowFragments
                                                              error:&jsonError];
        
        if(completion) completion(JSON);
    };
    
    NSURLSessionDataTask *task = [self.currentSession dataTaskWithURL:url
                                                    completionHandler:completionHandler];
    
    [task resume];
}

@end
