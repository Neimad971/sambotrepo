//
//  TrackCell.h
//  deezcovery
//
//  Created by Damien TALBOT on 13/02/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UIButton *playOrStop;

@end
