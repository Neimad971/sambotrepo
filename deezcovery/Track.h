//
//  Track.h
//  deezcovery
//
//  Created by Damien TALBOT on 20/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Track : NSObject

@property(strong,nonatomic) NSString *idTrack;
@property(strong,nonatomic) NSString *title;
@property(strong,nonatomic) NSString *duration;
@property(strong,nonatomic) NSString *preview;
@property(strong,nonatomic) NSString *albumCoverURL;

- (id) init;


@end
