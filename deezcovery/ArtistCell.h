//
//  ArtistCell.h
//  deezcovery
//
//  Created by Damien TALBOT on 26/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *artistName;
@property (weak, nonatomic) IBOutlet UILabel *albums;
@property (weak, nonatomic) IBOutlet UILabel *fans;

@end
