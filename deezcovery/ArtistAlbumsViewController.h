//
//  AlbumsViewController.h
//  deezcovery
//
//  Created by Hussam on 02/02/15.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Album.h"
#import "Artist.h"

@interface ArtistAlbumsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) Artist *artist;
@property (strong, nonatomic) NSMutableArray *albumsList;

@end
