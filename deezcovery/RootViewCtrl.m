//
//  RootViewCtrl.m
//  deezcovery
//
//  Created by Damien TALBOT on 17/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "RootViewCtrl.h"
#import "ArtistService.h"
#import "ArtistCell.h"
#import "Artist.h"

#import "Album.h"
#import "ArtistAlbumsViewController.h"

#define SEGUE_TO_DETAIL_ID  @"ListToDetail"

@interface RootViewCtrl ()


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) ArtistService *artistService;
@property (weak, nonatomic) IBOutlet UITextField *searchField;

@property (weak, nonatomic) Artist *selectedArtist;

@end




@implementation RootViewCtrl

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.artistsList = [[NSMutableArray alloc] init];
    
    self.artistService = [ArtistService sharedInstance];
    
    /*
    NSString *id = [self.artistService getArtistId:@"eminem"];
    NSLog(@"%@",id);
    self.artistsList = [[self.artistService getArtistRelatedToAnotherArtist:id] mutableCopy];
    */
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.artistsList.count;
}


-(ArtistCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArtistCell *cell = (ArtistCell *) [self.tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    
    
    Artist *a = self.artistsList[indexPath.row];
    
    if(a != nil)
    {
        NSString *whatEverVar = [a name];
        NSMutableString *nameWithPrefix = [[NSMutableString alloc]initWithString:@"Artist : "];
        [nameWithPrefix appendString:whatEverVar];
        [cell.artistName setText:nameWithPrefix];
        
        whatEverVar = [NSString stringWithFormat:@"%d", [a nbAlbums]];
        NSMutableString *nbAlbumsWithPrefix = [[NSMutableString alloc]initWithString:@"Albums : "];
        [nbAlbumsWithPrefix appendString:whatEverVar];
        [cell.albums setText:nbAlbumsWithPrefix];
        
        whatEverVar = [NSString stringWithFormat:@"%d", [a nbFans]];
        NSMutableString *nbFansWithPrefix = [[NSMutableString alloc]initWithString:@"Fans : "];
        [nbFansWithPrefix appendString:whatEverVar];
        [cell.fans setText:nbFansWithPrefix];
        
        NSURL *imageUrl = [NSURL URLWithString:a.artistImageURL];
        
        [self downloadImageWithURL:imageUrl completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                // change the image in the cell
                cell.imageView.image = image;
                [cell setNeedsLayout];
            }
        }];
    }
        
    
    
        
    
    return cell;
}

- (IBAction)didSearch:(id)sender
{
     NSLog(@"in didSearch");
     NSString *input = [self.searchField text];
     NSLog(@"%@ input : ",input);
    
    
    if(input == nil)
        return;
    
    
    NSString *id = [self.artistService getArtistId:input];
    NSLog(@"%@",id);
     
    self.artistsList = [[self.artistService getArtistRelatedToAnotherArtist:id] mutableCopy];
     
    NSLog(@"%@",self.artistsList);
    
    [self.tableView reloadData];
}

- (IBAction)didLongPressOnCell:(id)sender
{
     NSLog(@"loooooong press");
    
    CGPoint point = [sender locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    
    Artist *a = self.artistsList[indexPath.row];
    
    [self.searchField setText:[a name]];
    
    [self didSearch:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SEGUE_TO_DETAIL_ID]){
        ArtistAlbumsViewController *controller = [segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        self.selectedArtist = self.artistsList[indexPath.row];
        
        controller.artist = self.selectedArtist;
    }
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

@end
