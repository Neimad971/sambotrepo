//
//  ArtistCell.m
//  deezcovery
//
//  Created by Damien TALBOT on 26/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "ArtistCell.h"

@implementation ArtistCell

- (void)awakeFromNib
{
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
