//
//  ArtistService.h
//  deezcovery
//
//  Created by Damien TALBOT on 20/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArtistService : NSObject

+ (instancetype)sharedInstance;

- (NSString *) getArtistId:(NSString *)searchInput;

- (NSArray *) getArtistRelatedToAnotherArtist :(NSString *) otherArtistId;

- (NSString *) getArtistAlbums:(NSString *) idArtist;

- (NSString *) getTracksAlbum:(NSString *) tracklistUrl;

@end
