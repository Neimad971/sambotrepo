//
//  Album.h
//  deezcovery
//
//  Created by Damien TALBOT on 20/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Album : NSObject

@property(strong,nonatomic) NSString *idAlbum;
@property(strong,nonatomic) NSString *albumName;
@property(strong,nonatomic) NSString *tracklist;
@property(strong,nonatomic) NSString *albumCoverURL;


- (id) init;

@end
