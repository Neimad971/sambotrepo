//
//  Artist.h
//  deezcovery
//
//  Created by Damien TALBOT on 17/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Artist : NSObject

@property(strong,nonatomic) NSString *idArtist;
@property(strong,nonatomic) NSString *name;
@property(assign,nonatomic) int *nbAlbums;
@property(assign,nonatomic) int *nbFans;
@property(strong,nonatomic) NSString *artistImageURL;

- (id) init;




@end
