//
//  Track.m
//  deezcovery
//
//  Created by Damien TALBOT on 20/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "Track.h"

@implementation Track


- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        self.idTrack = nil;
        self.title = nil;
        self.duration = nil;
        self.preview = nil;
        self.albumCoverURL = nil;
    }
    
    return self;
}


@end
