//
//  RootViewCtrl.h
//  deezcovery
//
//  Created by Damien TALBOT on 17/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RootViewCtrl : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *artistsList;

@end
