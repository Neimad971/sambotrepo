//
//  TracksAlbumCtrl.m
//  deezcovery
//
//  Created by Damien TALBOT on 13/02/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "TracksAlbumCtrl.h"
#import "ArtistService.h"
#import "TrackCell.h"
#import "Track.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


@interface TracksAlbumCtrl ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *saveOrRemove;
@property (weak, nonatomic) ArtistService *artistService;
@property (strong, nonatomic) AVPlayer *player;
@end

@implementation TracksAlbumCtrl

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tracklist = [[NSMutableArray alloc] init];
    
    self.artistService = [ArtistService sharedInstance];
    

    self.tracklist = [[self.artistService getTracksAlbum:[self.album tracklist]] mutableCopy];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tracklist.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TrackCell *cell = (TrackCell *) [self.tableView dequeueReusableCellWithIdentifier:@"TrackCell"];
    
    Track *track = self.tracklist[indexPath.row];
    

    
    if(track != nil)
    {
        NSString *whatEverVar = [track title];
        NSMutableString *titleWithPrefix = [[NSMutableString alloc]initWithString:@"Title : "];
        [titleWithPrefix appendString:whatEverVar];
        [cell.title setText:titleWithPrefix];
        
        
        whatEverVar = [track duration];
        int minutes = [whatEverVar intValue] / 60;
        int secondes = [whatEverVar intValue] % 60;
        NSMutableString *durationWithPrefix = [[NSMutableString alloc]initWithString:@"Duration : "];
        [durationWithPrefix appendString:[NSString stringWithFormat:@"%d", minutes]];
        [durationWithPrefix appendString:@"."];
        [durationWithPrefix appendString:[NSString stringWithFormat:@"%d", secondes]];
        [durationWithPrefix appendString:@"min"];
        [cell.duration setText:durationWithPrefix];
      
    }
    
    NSURL *imageUrl = [NSURL URLWithString:self.album.albumCoverURL];
    
    [self downloadImageWithURL:imageUrl completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            // change the image in the cell
            cell.imageView.image = image;
            [cell setNeedsLayout];
        }
    }];

    
    return cell;
}

- (IBAction)didTouchPlay:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tableView];
    NSIndexPath *clickedIP = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Track *track = self.tracklist[clickedIP.row];
    NSString *soundPath = track.preview;
    NSURL *soundFileURL = [NSURL URLWithString:soundPath];
  
    self.player = [[AVPlayer alloc] initWithURL:soundFileURL];
    
    if (!self.player) {
        NSLog(@"Error");
    }
    [self.player setVolume:3];
    [self.player play];
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

@end
