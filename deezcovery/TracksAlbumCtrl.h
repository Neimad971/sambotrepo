//
//  TracksAlbumCtrl.h
//  deezcovery
//
//  Created by Damien TALBOT on 13/02/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"

@interface TracksAlbumCtrl : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *tracklist;
@property (strong, nonatomic) Album *album;

@end
