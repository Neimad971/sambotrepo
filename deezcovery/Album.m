//
//  Album.m
//  deezcovery
//
//  Created by Damien TALBOT on 20/01/2015.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "Album.h"

@implementation Album

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        self.idAlbum = nil;
        self.albumName = nil;
        self.tracklist = nil;
        self.albumCoverURL = nil;
    }
    
    return self;
}


@end
