//
//  AlbumsViewController.m
//  deezcovery
//
//  Created by Hussam on 02/02/15.
//  Copyright (c) 2015 Damien TALBOT. All rights reserved.
//

#import "ArtistAlbumsViewController.h"
#import "ArtistService.h"
#import "TracksAlbumCtrl.h"

@interface ArtistAlbumsViewController ()

@property (weak, nonatomic) ArtistService *artistService;
@property (weak, nonatomic) IBOutlet UITableView *albumsTableView;

@end

@implementation ArtistAlbumsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.albumsTableView.delegate = self;
    self.albumsTableView.dataSource = self;
    
    self.artistService = [ArtistService sharedInstance];
    self.albumsList = [[NSMutableArray alloc] init];
    
    [self setupDatas];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupDatas {
    
    NSString *id = self.artist.idArtist;
    
    self.albumsList = [[self.artistService getArtistAlbums:id] mutableCopy];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.albumsList.count;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [self.albumsTableView dequeueReusableCellWithIdentifier:@"myAlbumCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myAlbumCell"];
    }
    
    Album *a = self.albumsList[indexPath.row];
    
    cell.textLabel.text = a.albumName;
    
    // download the image asynchronously
    NSURL *imageUrl = [NSURL URLWithString:a.albumCoverURL];
    
    [self downloadImageWithURL:imageUrl completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            // change the image in the cell
            cell.imageView.image = image;
            [cell setNeedsLayout];
        }
    }];
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Tracklist"])
    {
        TracksAlbumCtrl *controller = [segue destinationViewController];
        
        NSIndexPath *indexPath = [self.albumsTableView indexPathForSelectedRow];
        controller.album = self.albumsList[indexPath.row];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Tracklist" sender:nil];
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}


@end
